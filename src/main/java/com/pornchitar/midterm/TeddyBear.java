package com.pornchitar.midterm;

public class TeddyBear extends DollStore {
    private  int choice = 2;
    private  int price = 80;

    public TeddyBear(String material, String name) {
        super(material, name);
    }
    public int getChoice(){
        return choice;
    }
    public int getPrice(){
        return price;
    }
}
