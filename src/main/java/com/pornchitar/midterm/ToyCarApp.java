package com.pornchitar.midterm;
import java.util.Scanner;
public class ToyCarApp {
    public static void main(String[] args) {
        ToyCar sedan = new ToyCar("Red", 'B', 1, 0);
        ToyCar truck = new ToyCar("Yellow", 'P', 5, 5);
        ToyCar ambulance = new ToyCar("White ", 'W', 10, 10);
    
        sedan.print();
        sedan.left();
        sedan.right();
        sedan.up();
        sedan.down();
        sedan.print();

        truck.print();
        truck.left();
        truck.right();
        truck.up();
        truck.down();
        truck.print();

        ambulance.print();
        ambulance.left();
        ambulance.right();
        ambulance.up();
        ambulance.down();
        ambulance.print();
        

        for (int y = ToyCar.Y_MIN; y <= ToyCar.Y_MAX; y++) {
            for (int x = ToyCar.X_MIN; x <= ToyCar.X_MAX; x++) {
                if (sedan.getX() == x && sedan.getY() == y) {
                    System.out.print(sedan.getSymbol());
                } else if (truck.getX() == x && truck.getY() == y) {
                    System.out.print(truck.getSymbol());
                } else if (ambulance.getX() == x && ambulance.getY() == y) {
                    System.out.print(ambulance.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
